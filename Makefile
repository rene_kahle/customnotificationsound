export THEOS_DEVICE_IP = localhost
export THEOS_DEVICE_PORT = 2222
#export THEOS_DEVICE_IP = 192.168.3.2
export ARCHS = armv7 arm64
export TARGET = iphone:latest:8.0

include theos/makefiles/common.mk

TWEAK_NAME = CustomNotificationSound
CustomNotificationSound_FILES = Tweak.xm CNSAppDetailController.mm
CustomNotificationSound_FRAMEWORKS = UIKit CoreGraphics AudioToolbox
CustomNotificationSound_PRIVATE_FRAMEWORKS = BulletinBoard SpringBoardUI Preferences

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
