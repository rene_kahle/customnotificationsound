#import "CNSAppDetailController.h"
#import "CustomNotificationSound.h"
#import <AudioToolbox/AudioToolbox.h>

extern "C" uint32_t notify_post(const char *);

static void FinishedPlaying(SystemSoundID systemSoundID, void *clientData)
{
	AudioServicesRemoveSystemSoundCompletion(systemSoundID);    
	AudioServicesDisposeSystemSoundID(systemSoundID);
}

static void PlaySound(const char *path)
{
	CFURLRef url = (CFURLRef)[NSURL fileURLWithPath:[NSString stringWithUTF8String:path]];
	SystemSoundID soundID;
	AudioServicesCreateSystemSoundID(url, &soundID);
	AudioServicesAddSystemSoundCompletion(soundID, NULL, NULL, FinishedPlaying, NULL);
	AudioServicesPlaySystemSound(soundID);
}

@implementation CNSAppDetailController

@synthesize soundSwitch;
@synthesize appTitle;
@synthesize valueOfNotificationType;
@synthesize pusherController;
@synthesize bundleIdentifier;
@synthesize chosenSoundPath;
@synthesize allSounds;

- (void)dealloc
{
	[soundSwitch release];
	soundSwitch = nil;

	[appTitle release];
	appTitle = nil;

	[valueOfNotificationType release];
	valueOfNotificationType = nil;

	[pusherController release];
	pusherController = nil;

	[bundleIdentifier release];
	bundleIdentifier = nil;

	[chosenSoundPath release];
	chosenSoundPath = nil;

	[allSounds release];
	allSounds = nil;

	[super dealloc];
}

- (instancetype)init
{
	if ((self = [super initWithStyle:UITableViewStyleGrouped]))
	{
		soundSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
		[self initAllSounds];
	}
	return self;
}

- (void)viewDidLoad
{
	self.title = NSLocalizedStringFromTableInBundle(@"CUSTOMSOUND", @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"👍" style:UIBarButtonItemStylePlain target:self action:@selector(like)] autorelease];
	[super viewDidLoad];	
}

- (void)like
{
	NSString *url = @"http://bbs.iosre.com/t/ios-app-reverse-engineering-the-worlds-1st-book-of-very-detailed-ios-app-reverse-engineering-skills/1117";
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)initAllSounds
{
	allSounds = [[NSMutableArray alloc] initWithCapacity:66];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *soundsPath = @"/System/Library/Audio/UISounds/";
	NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtPath:soundsPath];
	NSString *subpath;
	while (subpath = [enumerator nextObject])
	{
		if ([[subpath pathExtension] isEqualToString:@"caf"] || [[subpath pathExtension] isEqualToString:@"aif"] || [[subpath pathExtension] isEqualToString:@"m4a"] || [[subpath pathExtension] isEqualToString:@"m4r"] || [[subpath pathExtension] isEqualToString:@"mp3"] || [[subpath pathExtension] isEqualToString:@"wav"])
		{
			NSString *path = [NSString stringWithFormat:@"%@%@", soundsPath, subpath];
			if (![allSounds containsObject:path]) [allSounds addObject:path];
		}
	}
}

- (NSMutableArray *)allSections
{
	NSMutableArray *sections = [NSMutableArray arrayWithCapacity:66];
	for (NSString *path in self.allSounds)
	{
		NSString *section = [path stringByReplacingOccurrencesOfString:[path lastPathComponent] withString:@""];
		if (![sections containsObject:section]) [sections addObject:section];
	}
	return sections;
}

- (NSMutableArray *)soundsInSection:(NSInteger)sectionIndex
{
	NSMutableArray *sectionSounds = [NSMutableArray arrayWithCapacity:66];
	NSString *section = [self allSections][sectionIndex];
	for (NSString *path in self.allSounds)
	{
		if ([[path stringByReplacingOccurrencesOfString:[path lastPathComponent] withString:@""] isEqualToString:section] && ![sectionSounds containsObject:path]) [sectionSounds addObject:path];
	}
	return sectionSounds;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self allSections] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) return 1;
	return [[self soundsInSection:(section - 1)] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 0) return nil;
	return [[self allSections][section - 1] lastPathComponent];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if (section != 0) return NSLocalizedStringFromTableInBundle(@"NOTICE", @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);
	return NSLocalizedStringFromTableInBundle(@"CUSTOM", @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"any-cell"];
	if (cell == nil) cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"any-cell"] autorelease];
	for (UIView *subview in [cell.contentView subviews]) [subview removeFromSuperview];
	cell.textLabel.text = nil;
	cell.accessoryView = nil;
	cell.accessoryType = UITableViewCellAccessoryNone;

	if (indexPath.section == 0)
	{
		cell.textLabel.text = NSLocalizedStringFromTableInBundle(@"SOUND", @"Localizable", [NSBundle bundleWithPath:@"/Library/Application Support/CustomNotificationSound/CustomNotificationSound.bundle"], nil);
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.accessoryView = self.soundSwitch;
		self.soundSwitch.on = [[self.pusherController _valueOfNotificationType:0x10] boolValue];
		[self.soundSwitch addTarget:self action:@selector(saveSettings) forControlEvents:UIControlEventValueChanged];
	}
	else
	{
		NSString *soundPath = [self soundsInSection:(indexPath.section - 1)][indexPath.row];
		cell.textLabel.text = [soundPath lastPathComponent];
		if ([self.chosenSoundPath isEqualToString:soundPath]) cell.accessoryType = UITableViewCellAccessoryCheckmark;
		else cell.accessoryType = UITableViewCellAccessoryNone;
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section != 0)
	{
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
		NSMutableDictionary *settingsDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:@"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist"];
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
		if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
		{
			cell.accessoryType = UITableViewCellAccessoryNone;
			[settingsDictionary setObject:@"ORIGINAL" forKey:self.bundleIdentifier];
		}
		else
		{
			for (UITableViewCell *visibleCell in [tableView visibleCells]) visibleCell.accessoryType = UITableViewCellAccessoryNone;
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
			self.chosenSoundPath = [self soundsInSection:(indexPath.section - 1)][indexPath.row];
			PlaySound([self.chosenSoundPath UTF8String]);
			[settingsDictionary setObject:[self soundsInSection:(indexPath.section - 1)][indexPath.row] forKey:self.bundleIdentifier];
		}
		[settingsDictionary setObject:[NSNumber numberWithBool:self.soundSwitch.on] forKey:[self.bundleIdentifier stringByAppendingString:@"_"]];
		[settingsDictionary writeToFile:@"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist" atomically:YES];
		notify_post("com.naken.customnotificationsound.settingschanged");
	}
}

- (void)saveSettings
{
	NSMutableDictionary *settingsDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:@"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist"];
	[self.pusherController _setValue:[NSNumber numberWithBool:self.soundSwitch.on] notificationType:0x10];
	[settingsDictionary setObject:[NSNumber numberWithBool:self.soundSwitch.on] forKey:[self.bundleIdentifier stringByAppendingString:@"_"]];
	[settingsDictionary writeToFile:@"/var/mobile/Library/Preferences/com.naken.customnotificationsound.plist" atomically:YES];
	notify_post("com.naken.customnotificationsound.settingschanged");
}
@end
